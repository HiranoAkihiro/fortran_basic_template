program main
    implicit none

    integer(4) :: i,j,k,l,m,o,p,q,r,t    ! doループ用変数
    integer(4) :: n                      ! 全体剛性行列のサイズ
    !-------------------------input--------------------------------
    integer(4) :: nn                     !｛節点数｝
    integer(4) :: n_b,n_l,mdf            ! ｛境界条件数(bc)｝｛”(load)｝｛最大自由度数｝
    integer(4), allocatable :: elem(:,:) ! 節点、要素情報
    integer(4), allocatable :: elemx(:)  ! 節点id格納用配列
    integer(4) :: ne1,ne2                ! ｛要素数｝｛要素を構成する節点数｝
    real(8) :: E,nu
    real(8), allocatable :: Nm(:,:)      ! 節点座標
    real(8), allocatable :: Nmx(:,:)     ! 節点座標格納用配列
    real(8), allocatable :: Nm_3d(:,:,:) ! doループ用
    real(8), allocatable :: bc(:,:)
    real(8), allocatable :: load(:,:)
    !--------------------------------------------------------------
    real(8), allocatable :: Kem(:,:)     ! Ke:要素剛性マトリックス
    real(8), allocatable :: Kegm(:,:)    ! 全体剛性マトリックス
    real(8), allocatable :: Kegmx(:,:)   ! 全体剛性配置用
    !---------------------ソルバー部分-----------------------------
    real(8), allocatable :: A(:,:)       ! 係数行列
    real(8), allocatable :: b(:)         ! 右辺ベクトル
    real(8), allocatable :: x(:)         ! 解ベクトル（ここでは変位ベクトルに相当）
    !--------------------------------------------------------------
    !-----------------------誤差評価部分----------------------------
    real(8), allocatable :: x_th(:) ! 変位理論解
    real(8) :: el2 ! L2誤差
    !--------------------------------------------------------------

    !************.dat読み込み**************
    call read_node(Nm,nn)
    call read_elem(elem,ne1,ne2)
    call read_load(load,n_l)
    call read_bc(bc,n_b,mdf)
    call read_input(E,nu)
    !**************************************

    n = nn*mdf

    allocate(Nm_3d(ne1,nn,2))
    allocate(Nmx(nn,2))
    allocate(elemx(ne2))
    allocate(Kegm(n,n))
    allocate(Kegmx(n,n))
    allocate(A(n,n))
    allocate(b(n))
    allocate(x(n))
    allocate(x_th(n))
    allocate(Kem(mdf*ne2,mdf*ne2))
    elemx=0.0d0
    Nm_3d=0.0d0
    Nmx=0.0d0
    elemx=0.0d0
    Kegm=0.0d0
    Kegmx=0.0d0
    A=0.0d0
    b=0.0d0
    x=0.0d0
    x_th=0.0d0
    Kem=0.0d0

    !*******ローカル節点をdoループで回すための３次元配列******
    do i=1,ne1
        do j=1,ne2
            do k=1,2
                Nm_3d(i,j,k) = Nm(elem(i,j),k)
            enddo
        enddo
    enddo
    !*********************************************************

    !********************全体剛性行列計算*********************
    do i=1,ne1
        do j=1,ne2
            do k=1,2
                Nmx(j,k) = Nm_3d(i,j,k)
            enddo
        enddo

        do t=1,ne2
            elemx(t) = elem(i,t)
        enddo

        !-----要素剛性行列計算-------
        call lsm_calculation(Nmx,Kem)
        !----------------------------
        Kegmx=0.0d0

        !-------ローカル→グローバルにした時の配置------------
        do o=1,ne2
            do p=1,ne2
                do q=1,2
                    do r=1,2
                        Kegmx(elemx(o)*2-2+q,elemx(p)*2-2+r) = Kem(o*2-2+q,p*2-2+r) 
                    enddo
                enddo
            enddo
        enddo
        !----------------------------------------------------

        do l=1,n
            do m=1,n
                Kegm(l,m) = Kegm(l,m) + Kegmx(l,m)
            enddo
        enddo  
    enddo
    !********************************************************

    A = Kegm

    !***********境界条件適用*************
    call Dirichlet(A,b)
    ! call Neumann(b)
    !***********************************

    !**********線形ソルバー**************
    call linear_solver_CG(A,b,x,n)
    !***********************************

    ! call theoretical_solution(x_th)
    ! write(*,*)'変位ベクトル（解ベクトル）                                                  理論値'
    ! do i=1,n
    !     if(mod(i,2) == 0)then
    !         cycle
    !     endif
    !     write(*,*)x(i),x(i+1),x_th(i),x_th(i+1)
    ! enddo

    call output_vtk(Nm,x,nn,ne1,elem,ne2)

    call error_evaluation(el2,Nm,elem,x)
    write(*,*)el2

contains
!***********************誤差評価(renew)*****************************
subroutine error_evaluation(el2,Nm,elem,x)
    implicit none
    integer(4) :: i,j,k,l,m 
    integer(4), allocatable :: elem(:,:) ! 節点、要素情報
    real(8) :: eta, xi                  !
    real(8) :: eta_array(2),xi_array(2) !　eta,xiのそれぞれ２通りを格納する行列
    real(8) :: N_mat(2,8) ! 要素内の変位(ξ,η)を求めるための形状関数行列
    real(8) :: u_th(2),u_fem(2) ! 積分点に関する理論解とFEM解
    real(8) :: u_d(2) ! 理論解とFEM解の差分
    real(8) :: norm_squared_ud ! 差分のノルムの2乗
    real(8) :: norm_squared_uth ! 理論解のノルムの二乗
    real(8) :: integration_point(2) ! 積分点の座標(ξ,η)
    real(8) :: node_vector(8) ! 要素節点座標（ベクトル）
    real(8) :: displacement_vector(8) ! 要素節点変位のFEM解（ベクトル）
    real(8) :: r ! 極座標、原点からの距離
    real(8) :: a0 ! 円孔半径
    real(8) :: theta_bc ! 極座標、角度
    real(8) :: mu ! μ　せん断弾性率（せん断弾性係数）
    real(8) :: kappa ! κ　(Kolosov constant)
    real(8) :: bc1,bc2,bc3,bc4,bc5,bc6,bc7
    real(8) :: detJ ! 行列式
    real(8) :: Nm_elem(4,2) ! 要素節点座標（行列）
    real(8) :: el2,el2_d,el2_n ! L2誤差、分母、分子
    real(8), allocatable :: Nm(:,:)      ! 節点座標
    real(8), allocatable :: x(:)         ! 解ベクトル（ここでは変位ベクトルに相当）

    N_mat=0.0d0

    kappa = (3.0d0 - nu) / (1.0d0 + nu)
    mu = dble(E)/(2.0d0*(1+nu))
    a0 = 1.0d0

    norm_squared_ud=0.0d0
    norm_squared_uth=0.0d0
    el2_d=0.0d0
    el2_n=0.0d0
    el2=0.0d0

    bc1 = a0/(8.0d0*mu)

    eta_array(1) = -0.57735d0
    eta_array(2) = 0.57735d0
    xi_array(1) = -0.57735d0
    xi_array(2) = 0.57735d0

    do k=1,ne1
        norm_squared_uth = 0.0d0
        norm_squared_ud = 0.0d0

        do l=1,4
            node_vector(l*2-1) = Nm(elem(k,l),1)
            node_vector(l*2) = Nm(elem(k,l),2)
        enddo

        do l=1,4
            displacement_vector(l*2-1) = x(elem(k,l)*2-1)
            displacement_vector(l*2) = x(elem(k,l)*2)
        enddo

        do i=1,2
            do j=1,2
                integration_point=0.0d0
                u_fem=0.0d0

                eta = eta_array(i)
                xi = xi_array(j)

                N_mat(1,1) = (1-xi)*(1-eta)/4
                N_mat(1,3) = (1+xi)*(1-eta)/4
                N_mat(1,5) = (1+xi)*(1+eta)/4
                N_mat(1,7) = (1-xi)*(1+eta)/4
                N_mat(2,2) = (1-xi)*(1-eta)/4
                N_mat(2,4) = (1+xi)*(1-eta)/4
                N_mat(2,6) = (1+xi)*(1+eta)/4
                N_mat(2,8) = (1-xi)*(1+eta)/4

                do l=1,2
                    do m=1,8
                        integration_point(l) = integration_point(l) + N_mat(l,m) * node_vector(m)
                    enddo
                enddo

                r = sqrt(integration_point(1)**2.0d0 + integration_point(2)**2.0d0)
                theta_bc = atan(integration_point(2)/integration_point(1))

                bc2 = r*(kappa+1.0d0)*cos(theta_bc)/a0
                bc3 = 2.0d0*a0*((1.0d0+kappa)*cos(theta_bc)+cos(theta_bc*3.0d0))/r
                bc4 = 2.0d0*(a0**3.0d0)*cos(theta_bc*3.0d0)/(r**3.0d0)
                bc5 = r*(kappa-3.0d0)*sin(theta_bc)/a0
                bc6 = 2.0d0*a0*((1.0d0-kappa)*sin(theta_bc)+sin(theta_bc*3.0d0))/r
                bc7 = 2.0d0*(a0**3.0d0)*sin(theta_bc*3.0d0)/(r**3.0d0)

                u_th(1) = bc1*(bc2+bc3-bc4)
                u_th(2) = bc1*(bc5+bc6-bc7)

                do l=1,2
                    do m=1,8
                        u_fem(l) = u_fem(l) + N_mat(l,m)*displacement_vector(m)
                    enddo
                enddo

                do l=1,2
                    u_d(l) = u_th(l) - u_fem(l)
                enddo

                do l=1,4
                    Nm_elem(l,1) = Nm(elem(k,l),1)
                    Nm_elem(l,2) = Nm(elem(k,l),2)
                enddo

                call calculate_detJ(detJ,Nm_elem,eta,xi)

                do l=1,2
                    norm_squared_ud = norm_squared_ud + (u_d(l)**2.0d0)*detJ
                enddo

                do l=1,2
                    norm_squared_uth = norm_squared_uth + (u_th(l)**2.0d0)*detJ
                enddo
            enddo
        enddo
        el2_d = el2_d + norm_squared_uth
        el2_n = el2_n + norm_squared_ud
    enddo

    el2 = sqrt(el2_n)/sqrt(el2_d)

end subroutine error_evaluation         

subroutine calculate_detJ(detJ,Nm_elem,eta,xi)
    implicit none
    integer(4) i,j,k
    real(8) :: eta,xi
    real(8) :: detJ
    real(8) :: Nm_elem(4,2) ! 要素節点座標（行列）
    real(8) :: Jmatrix(2,2)
    real(8) :: s(2,4)
 
    s(1,1)=-(1.0d0-eta)/4.0d0
    s(1,2)=(1.0d0-eta)/4.0d0
    s(1,3)=(1.0d0+eta)/4.0d0
    s(1,4)=-(1.0d0+eta)/4.0d0
    s(2,1)=-(1.0d0-xi)/4.0d0
    s(2,2)=-(1.0d0+xi)/4.0d0
    s(2,3)=(1.0d0+xi)/4.0d0
    s(2,4)=(1.0d0-xi)/4.0d0

    do i=1,2
        do j=1,2
            do k=1,4
                Jmatrix(i,j) = Jmatrix(i,j) + s(i,k)*Nm_elem(k,j)
            enddo
        enddo
    enddo

    detJ = Jmatrix(1,1) * Jmatrix(2,2) - Jmatrix(1,2) * Jmatrix(2,1)

end subroutine calculate_detJ
!***********************************************************

!-------------------------理論解-------------------------------
subroutine theoretical_solution(x_th)
    implicit none
    integer(4) :: i,j
    integer(4) :: nee,nnee ! 節点数,分割数
    real(8), allocatable :: x_th(:)
    real(8) :: mu ! μ　せん断弾性率（せん断弾性係数）
    real(8) :: kappa ! κ　(Kolosov constant)
    real(8) :: theta_bc ! Θ 極座標、角度
    real(8) :: r ! 極座標、距離
    real(8) :: pi
    real(8) :: a0 ! 円孔半径
    real(8) :: bc1,bc2,bc3,bc4,bc5,bc6,bc7
    real(8) :: lc ! 辺の長さ
    
    kappa = (3.0d0 - nu) / (1.0d0 + nu)
    mu = dble(E)/(2.0d0*(1+nu))
    pi = 2.0d0 * acos(0.0d0)
    a0 = 1.0d0
    lc = 10.0d0


    !*****writeout.f90からの使いまわしの都合上、定義しなおし******
    nee = nn ! 節点数
    nnee = int(sqrt(dble(nee))-1) ! 分割数
    !**********************************************************

    bc1 = (a0/(8.0d0*mu))

    do i=1,nnee+1
        if(i < nnee/2+1)then
            theta_bc = ((pi/2.0d0)/dble(nnee))*dble(i-1)

            do j=1,nnee+1
                r = dble(j-1)*(lc/cos(theta_bc)-1.0d0)/dble(nnee) + 1.0d0

                bc2 = r*(kappa+1.0d0)*cos(theta_bc)/a0
                bc3 = 2.0d0*a0*((1.0d0+kappa)*cos(theta_bc)+cos(theta_bc*3.0d0))/r
                bc4 = 2.0d0*(a0**3.0d0)*cos(theta_bc*3.0d0)/(r**3.0d0)
                bc5 = r*(kappa-3.0d0)*sin(theta_bc)/a0
                bc6 = 2.0d0*a0*((1.0d0-kappa)*sin(theta_bc)+sin(theta_bc*3.0d0))/r
                bc7 = 2.0d0*(a0**3.0d0)*sin(theta_bc*3.0d0)/(r**3.0d0)

                x_th(j*2-1+(i-1)*(nnee+1)*2) = bc1*(bc2+bc3-bc4)
                x_th(j*2+(i-1)*(nnee+1)*2) = bc1*(bc5+bc6-bc7)
            enddo
        else
            theta_bc = ((pi/2.0d0)/dble(nnee))*dble(i-1)

            do j=1,nnee+1
                r = dble(j-1)*(lc/sin(theta_bc)-1.0d0)/dble(nnee) + 1.0d0

                bc2 = r*(kappa+1.0d0)*cos(theta_bc)/a0
                bc3 = 2.0d0*a0*((1.0d0+kappa)*cos(theta_bc)+cos(theta_bc*3.0d0))/r
                bc4 = 2.0d0*(a0**3.0d0)*cos(theta_bc*3.0d0)/(r**3.0d0)
                bc5 = r*(kappa-3.0d0)*sin(theta_bc)/a0
                bc6 = 2.0d0*a0*((1.0d0-kappa)*sin(theta_bc)+sin(theta_bc*3.0d0))/r
                bc7 = 2.0d0*(a0**3.0d0)*sin(theta_bc*3.0d0)/(r**3.0d0)

                x_th(j*2-1+(i-1)*(nnee+1)*2) = bc1*(bc2+bc3-bc4)
                x_th(j*2+(i-1)*(nnee+1)*2) = bc1*(bc5+bc6-bc7)
            enddo
        endif
    enddo

end subroutine theoretical_solution
!----------------------------------------------------------
!**********************************************************

!****************vtkファイル出力****************************
subroutine output_vtk(Nm,x,nn,ne1,elem,ne2)
    implicit none
    integer(4) :: i
    integer(4) :: nn
    integer(4) :: ne1,ne2
    real(8), allocatable :: Nm(:,:)
    real(8), allocatable :: x(:)
    integer(4), allocatable :: elem(:,:)

    open(30,file="circular_hole.vtk",status="replace")
        write(30,'(a)')'# vtk DataFile Version 4.1'
        write(30,'(a)')'Title'
        write(30,'(a)')'ASCII'
        write(30,'(a)')'DATASET UNSTRUCTURED_GRID'
        write(30,*)''
        write(30,'(a,a,i0,a,a)')'POINTS',' ',nn,' ','float'
        do i=1,nn
            write(30,'(3f12.6)')Nm(i,1),Nm(i,2),0.0d0
        enddo
        write(30,*)''
        write(30,'(a,a,i0,a,i0)')'CELLS',' ',ne1,' ',(ne2+1)*ne1
        do i=1,ne1
            write(30,'(i0,a,i0,a,i0,a,i0,a,i0)')ne2,' ',elem(i,1)-1,' ',elem(i,2)-1,' ',elem(i,3)-1,' ',elem(i,4)-1
        enddo
        write(30,*)''
        write(30,'(a,a,i0)')'CELL_TYPES',' ',ne1
        do i=1,ne1
            write(30,'(i0)')9
        enddo
        write(30,*)''
        write(30,'(a,a,i0)')'POINT_DATA',' ',nn
        write(30,'(a)')'VECTORS Displacement float'
        do i=1,nn*2
            if(mod(i,2) == 0)then
                cycle
            endif
            write(30,'(3f19.16)')x(i),x(i+1),0.0d0
        enddo
    close(30)
end subroutine output_vtk
!**********************************************************

!****************ノイマン境界条件の付与**********************
subroutine Neumann(b)
    implicit none
    integer(4) :: i
    real(8), allocatable :: b(:)      ! 右辺ベクトル

    do i=1,n_l
        b(int(load(i,1))*2-(2-int(load(i,2)))) = load(i,3)
    enddo
end subroutine Neumann
!***********************************************************

!******************ディリクレ境界条件の付与******************
subroutine Dirichlet(A,b)
    implicit none
    integer(4) :: i,j
    real(8), allocatable :: A(:,:)  ! 係数行列
    real(8), allocatable :: b(:)

    do i=1,n_b 
        do j=1,n
            b(j) = b(j) - A(j,int(bc(i,1))*mdf-(mdf-int(bc(i,2))))*bc(i,3)
        enddo
        do j=1,n
            A(j,(int(bc(i,1))*mdf)-(mdf-int(bc(i,2)))) = 0.0d0
            A((int(bc(i,1))*mdf)-(mdf-int(bc(i,2))),j) = 0.0d0
        enddo
        A(int(bc(i,1))*mdf-(mdf-int(bc(i,2))),int(bc(i,1))*mdf-(mdf-int(bc(i,2)))) = 1.0d0
        b(int(bc(i,1))*mdf-(mdf-int(bc(i,2)))) = bc(i,3)
    enddo
end subroutine Dirichlet
!**************************************************************

!******************要素剛性行列計算****************************
subroutine lsm_calculation(Nmx,Kem)
    implicit none
    integer(4) :: i,j,k,l               !
    real(8), allocatable :: Nmx(:,:)    !
    real(8) :: eta, xi                  !
    real(8) :: eta_array(2),xi_array(2) !
    real(8) :: Jm(2,2)                  !　Jマトリックス
    real(8) :: Jminv(2,2)               !　Jマトリックスの逆行列
    real(8) :: detJm                    ! Jマトリックスの行列式
    real(8) :: Dm(3,3)                  !　Dマトリックス
    real(8) :: n1(2),n2(2),n3(2),n4(2)  !　Bマトリックスの要素（形状関数N₁(n1)～N₄(n4)について、1要素目：（∂N/∂x）、2要素目：（∂N/∂y））
    real(8) :: Bm(3,8)                  ! Bマトリックス
    real(8) :: BTm(8,3)                 ! Bマトリックスの転置
    real(8) :: BTDm(8,3)                !　[BT][D]
    real(8) :: BTDBm(8,8)               !　[BT][D][B](Keの積分する前)
    real(8) :: Kem(8,8)                 ! Ke:要素剛性マトリックス

    eta_array(1) = -0.57735d0
    eta_array(2) = 0.57735d0
    xi_array(1) = -0.57735d0
    xi_array(2) = 0.57735d0

    Kem=0.0d0

    do i=1,2
        do j=1,2
            eta = eta_array(i)
            xi = xi_array(j)

            call Jmatrix(eta,xi,Jm,Nmx,ne2)
            call inverse(Jminv,Jm,detJm)
            call n1_pd(n1,Jminv,eta,xi)
            call n2_pd(n2,Jminv,eta,xi)
            call n3_pd(n3,Jminv,eta,xi)
            call n4_pd(n4,Jminv,eta,xi)
            call Bmatrix(Bm,n1,n2,n3,n4)
            call Dmatrix(Dm)
            call B_transposed(BTm,Bm)
            call BT_D_calculation(BTDm,Dm,BTm)
            call BTD_B_calculation(BTDBm,BTDm,Bm)
            do k=1,8
                do l=1,8
                    Kem(k,l) = Kem(k,l) + BTDBm(k,l)*detJm
                enddo
            enddo
        enddo
    enddo
end subroutine lsm_calculation
!***********************************************************

!*******************共役勾配法******************************
subroutine linear_solver_CG(A,b,x,n)
    implicit none
    integer(4) :: i,j,n
    real(8), allocatable :: A(:,:) !　係数行列
    real(8), allocatable :: b(:)   !　右辺ベクトル
    real(8), allocatable :: x(:)   !　解ベクトル（ここでは変位ベクトルに相当）
    real(8), allocatable :: r(:)   !　残差ベクトル
    real(8), allocatable :: p(:)   !　
    real(8), allocatable :: q(:)   !　
    real(8) :: bnrm2,rho,beta,rho1,dnrm2,c1,alpha,resid !　
    real(8) :: eps
    integer(4) :: iter,itermax
    itermax=1000
    eps=1.0E-8

    allocate(r(n))
    allocate(p(n))
    allocate(q(n))

    do i=1,n
        r(i) = b(i)
        do j=1,n
            r(i) = r(i) - A(i,j)*x(j)
        enddo
    enddo

    bnrm2 = 0.0d0
    do i=1,n
        bnrm2 = bnrm2 + b(i) ** 2.0d0
    enddo

    rho1 = 0.0d0
    do iter=1,itermax

        rho = 0.0d0
        do i=1,n
            rho = rho + r(i)*r(i)
        enddo

        if(iter.eq.1) then
            do i=1,n
                p(i) = r(i)
            enddo
        else
            beta = rho / rho1
            do i=1,n
                p(i) = r(i) + beta * p(i)
            enddo
        endif

        do i=1,n
            q(i) = 0.0d0
            do j=1,n
                q(i) = q(i) + A(i,j) * p(j)
            enddo
        enddo


        c1=0.0d0
        do i=1,n
            c1 = c1 + p(i) * q(i)
        enddo
        alpha = rho / c1

        do i=1,n
            x(i) = x(i) + alpha * p(i)
            r(i) = r(i) - alpha * q(i)
        enddo

        dnrm2 = 0.0d0
        do i=1,n
            dnrm2 = dnrm2 + r(i) ** 2
        enddo

        resid = dsqrt(dnrm2/bnrm2)

        if(resid.le.eps) exit
        rho1 =rho

    enddo
end subroutine linear_solver_CG

!**********************************************************

!***********BTD_B_calculation（[BTD][B]の計算）************
subroutine BTD_B_calculation(BTDBm,BTDm,Bm)
    implicit none
    integer(4) :: i,j,k
    real(8) :: BTDBm(8,8)
    real(8) :: Bm(3,8)
    real(8) :: BTDm(8,3)
    BTDBm=0.0d0
    do i=1,8
        do j=1,8
            do k=1,3
                BTDBm(i,j) = BTDBm(i,j) + BTDm(i,k) * Bm(k,j)
            enddo
        enddo
    enddo

end subroutine BTD_B_calculation

!*********************************************************

!**********BT_D_calculation（[BT][D]の計算）***************
subroutine BT_D_calculation(BTDm,Dm,BTm)
    implicit none
    integer(4) :: i,j,k
    real(8) :: BTDm(8,3)
    real(8) :: BTm(8,3)
    real(8) :: Dm(3,3)
    BTDm = 0.0d0
    do i=1,8
        do j=1,3
            do k=1,3
                BTDm(i,j) = BTDm(i,j) + BTm(i,k) * Dm(k,j)
            enddo
        enddo
    enddo
end subroutine BT_D_calculation

!*********************************************************

!************B_transposed（Bの転置計算）*******************
subroutine B_transposed(BTm,Bm)
    implicit none
    integer(4) :: i,j
    real(8) :: BTm(8,3)
    real(8) :: Bm(3,8)
    BTm=0.0d0
    do i=1,3
        do j=1,8
            BTm(j,i)=Bm(i,j)
        enddo
    enddo
end subroutine B_transposed

!*********************************************************

!**************Bmatrix（Bマトリックス）********************
subroutine Bmatrix(Bm,n1,n2,n3,n4)
    implicit none
    integer(4) :: i 
    real(8) :: Bm(3,8)
    real(8) :: n1(2),n2(2),n3(2),n4(2)
    Bm(1,1)=n1(1)
    Bm(1,3)=n2(1)
    Bm(1,5)=n3(1)
    Bm(1,7)=n4(1)
    Bm(2,2)=n1(2)
    Bm(2,4)=n2(2)
    Bm(2,6)=n3(2)
    Bm(2,8)=n4(2)
    Bm(3,1)=n1(2)
    Bm(3,2)=n1(1)
    Bm(3,3)=n2(2)
    Bm(3,4)=n2(1)
    Bm(3,5)=n3(2)
    Bm(3,6)=n3(1)
    Bm(3,7)=n4(2)
    Bm(3,8)=n4(1)
    do i=1,4
        Bm(1,2*i)=0.0d0
        Bm(2,2*i-1)=0.0d0
    enddo
end subroutine Bmatrix

!*********************************************************

!***********Dマトリックスの定義****************************
subroutine Dmatrix(Dm)
    implicit none
    integer(4) :: k,l
    real(8) :: Dm(3,3)

    Dm = 0.0d0
    Dm(1,1) = 1.0d0 / (1.0d0 - nu)
    Dm(1,2) = nu / (1.0d0 - nu)
    Dm(2,1) = nu / (1.0d0 - nu)
    Dm(2,2) = 1.0d0 / (1.0d0 - nu)
    Dm(3,3) = 1.0d0 / 2.0d0
    do k = 1,2
        Dm(k,3) = 0.0d0
        Dm(3,k) = 0.0d0
    enddo
    do k=1,3
        do l=1,3
            Dm(k,l)=Dm(k,l)*(E/(1+nu))
        enddo
    enddo

end subroutine Dmatrix

!*********************************************************

!******************n1（B要素計算）**************************
subroutine n1_pd(n1,Jminv,eta,xi)
    implicit none
    real(8) :: eta, xi
    real(8) :: n1(2)
    real(8) :: Jminv(2,2)
    n1=0.0d0
    n1(1)=(Jminv(1,1) * (-(1.0d0-eta)/4.0d0)) + (Jminv(1,2) * (-(1.0d0-xi)/4.0d0))
    n1(2)=(Jminv(2,1) * (-(1.0d0-eta)/4.0d0)) + (Jminv(2,2) * (-(1.0d0-xi)/4.0d0))
end subroutine n1_pd
!**********************************************************

!******************n2（B要素計算）**************************
subroutine n2_pd(n2,Jminv,eta,xi)
    implicit none
    real(8) :: eta, xi
    real(8) :: n2(2)
    real(8) :: Jminv(2,2)
    n2=0.0d0
    n2(1)=(Jminv(1,1) * (1.0d0-eta)/4.0d0) + (Jminv(1,2) * -(1.0d0+xi)/4.0d0)
    n2(2)=(Jminv(2,1) * (1.0d0-eta)/4.0d0) + (Jminv(2,2) * -(1.0d0+xi)/4.0d0)
end subroutine n2_pd
!**********************************************************

!******************n3（B要素計算）**************************
subroutine n3_pd(n3,Jminv,eta,xi)
    implicit none
    real(8) :: eta, xi
    real(8) :: n3(2)
    real(8) :: Jminv(2,2)
    n3=0.0d0
    n3(1)=(Jminv(1,1) * (1.0d0+eta)/4.0d0) + (Jminv(1,2) * (1.0d0+xi)/4.0d0)
    n3(2)=(Jminv(2,1) * (1.0d0+eta)/4.0d0) + (Jminv(2,2) * (1.0d0+xi)/4.0d0)
end subroutine n3_pd
!**********************************************************

!******************n4（B要素計算）**************************
subroutine n4_pd(n4,Jminv,eta,xi)
    implicit none
    real(8) :: eta, xi
    real(8) :: n4(2)
    real(8) :: Jminv(2,2)
    n4=0.0d0
    n4(1)=(Jminv(1,1) * -(1.0d0+eta)/4.0d0) + (Jminv(1,2) * (1.0d0-xi)/4.0d0)
    n4(2)=(Jminv(2,1) * -(1.0d0+eta)/4.0d0) + (Jminv(2,2) * (1.0d0-xi)/4.0d0)
end subroutine n4_pd
!**********************************************************

!****************inverse（J逆行列計算）*********************
subroutine inverse(Jminv,Jm,detJm)
    implicit none
    real(8) :: j11, j12, j21, j22, detJm
    real(8) :: Jminv(2,2)
    real(8) :: Jm(2,2)

    j11 = Jm(1,1)
    j12 = Jm(1,2)
    j21 = Jm(2,1)
    j22 = Jm(2,2)

    detJm = 0.0d0

    detJm = j11 * j22 - j12 * j21

    Jminv(1,1) = j22 / detJm
    Jminv(1,2) = -(j12 / detJm)
    Jminv(2,1) = -(j21 / detJm)
    Jminv(2,2) = j11 / detJm
end subroutine inverse

!*********************************************************

!*****************Jマトリックスの演算**********************
subroutine Jmatrix(eta,xi,Jm,Nmx,ne2)
    implicit none
    integer(4) :: k,l,m
    real(8) :: s(2,4)
    real(8) :: Jm(2,2)
    real(8) :: eta, xi 
    real(8), allocatable :: Nmx(:,:)
    integer(4) :: ne2
    s=0.0d0
    Jm=0.0d0
    s(1,1)=-(1.0d0-eta)/4.0d0
    s(1,2)=(1.0d0-eta)/4.0d0
    s(1,3)=(1.0d0+eta)/4.0d0
    s(1,4)=-(1.0d0+eta)/4.0d0
    s(2,1)=-(1.0d0-xi)/4.0d0
    s(2,2)=-(1.0d0+xi)/4.0d0
    s(2,3)=(1.0d0+xi)/4.0d0
    s(2,4)=(1.0d0-xi)/4.0d0
    do k=1,2
        do l=1,2
            do m=1,ne2
                Jm(k,l) = Jm(k,l) + s(k,m) * Nmx(m,l)
            enddo
        enddo
    enddo
end subroutine Jmatrix

!*********************************************************

!********read_input（解析パラメータの読み込み）*************
subroutine read_input(E,nu)
    implicit none
    real(8) :: E,nu
    open(10,file="input.dat",status="old")
        read(10,*)E
        read(10,*)nu
    close(10)
end subroutine read_input

!*********************************************************

!**************read_node（節点読み込み）*******************
subroutine read_node(Nm,nn)
    implicit none
    integer(4) :: i,nn
    real(8),allocatable :: Nm(:,:)
    open(11,file="node.dat",status="old")
        read(11,*)nn
        allocate(Nm(nn,2))
        
        do i=1,nn
            read(11,*)Nm(i,1),Nm(i,2)
        enddo
    close(11)

end subroutine read_node

!*********************************************************

!******************read_elem（要素の読み込み）*************
        subroutine read_elem(elem,ne1,ne2)
            implicit none
            integer(4), allocatable :: elem(:,:)
            integer(4) :: ne1,ne2
            open(12,file="elem.dat",status="old")
            read(12,*)ne1,ne2
                allocate(elem(ne1,ne2))
                do i=1,ne1
                    read(12,*)elem(i,1:ne2)
                enddo
           close(12)
        end subroutine read_elem

!*********************************************************

!*********read_bc（ディリクレ境界条件の読み込み）***********
        subroutine read_bc(bc,n_b,mdf)
           implicit none
           integer(4) :: i,n_b,mdf
           real(8), allocatable :: bc(:,:)
           open(13,file="bc.dat",status="old")
               read(13,*)n_b,mdf !｛境界条件数｝｛最大自由度数｝
               allocate(bc(n_b,3)) ! ｛節点id｝｛自由度方向｝｛変位値｝
               bc=0.0d0
               do i=1,n_b
                   read(13,*)bc(i,1),bc(i,2),bc(i,3)
               enddo
           close(13)
        end subroutine read_bc

!*********************************************************

!***********read_load（ノイマン境界条件の読み込み）*********
        subroutine read_load(load,n_l)
          implicit none
           integer(4) :: i, n_l,n_mdf_l
           real(8), allocatable :: load(:,:)
           open(14,file="load.dat",status="old")
               read(14,*)n_l,n_mdf_l
               allocate(load(n_l,3))
               do i=1,n_l
                   read(14,*)load(i,1),load(i,2),load(i,3)
               enddo
           close(14)
        end subroutine read_load

!*********************************************************

end program main