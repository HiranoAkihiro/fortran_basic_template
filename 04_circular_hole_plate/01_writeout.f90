program main
    implicit none
    integer(4) :: i,j
    integer(4) :: n,nn ! 節点数、分割数
    integer(4) :: ne1,ne2 ! 要素数、要素を構成する節点数
    integer(4) :: temp ! 列入れ替え用変数
    integer(4), allocatable :: elem(:,:) ! 要素情報
    integer(4), allocatable :: elemx(:,:) ! 要素情報(仮)
    integer(4) :: n_b ! 境界条件数(bc)
    integer(4) :: n_l ! 境界条件数(load)
    integer(4) :: mdf ! 最大自由度数
    integer(4) :: E ! ヤング率
    real(8) :: nu ! ポアソン比　ν
    real(8), allocatable :: bc(:,:) ! ディリクレ境界条件
    real(8), allocatable :: bc_f(:,:) ! ディリクレ強制変位
    real(8), allocatable :: load(:,:) ! ノイマン境界条件
    real(8) :: mc ! modカウント用
    real(8) :: l ! 平板の辺の長さ
    real(8) :: r ! 円孔半径
    real(8), allocatable :: Nm(:,:)
    real(8) :: theta ! 放射状角度
    real(8) :: h ! 斜辺
    real(8) :: hn ! 分割長さ
    real(8) :: x,y ! x座標、y座標
    real(8) :: pi ! 円周率
    real(8) :: lp ! 載荷面における節点間の距離
    real(8) :: q ! 平板上面にかかる分布荷重（N/m）
    real(8) :: p ! 頂点にかかる等価節点力
    real(8) :: mu ! μ　せん断弾性率（せん断弾性係数）
    real(8) :: kappa ! κ　(Kolosov constant)
    real(8) :: theta_bc ! Θ 極座標、角度
    real(8) :: a ! 極座標、距離
    real(8) :: bc1,bc2,bc3,bc4,bc5,bc6,bc7



    mdf=2

!********************input****************************
    E = 100000
    nu = 0.3d0
    open(11,file="input.dat",status="replace")
        write(11,'(i0)')E
        write(11,'(f3.1)')nu
    close(11)
!*****************************************************

!**********************node***************************

    nn = 2*30 ! nnは必ず偶数！でないと対角線が引かれない。
    n = (nn+1)*(nn+1)
    allocate(Nm(n,2))
    pi = 2.0d0 * acos(0.0d0)
    l=10.0d0
    r=1.0d0
    Nm=0.0d0

    do i=1,nn+1
        theta = (pi/(2.0d0*nn))*(dble(i)-1.0d0)
        if(i <= nn/2)then
            h = (l/cos(theta)) - r
            hn = h/nn
            do j=1,nn+1
                x = (r + hn*(dble(j)-1.0d0))*cos(theta)
                y = (r + hn*(dble(j)-1.0d0))*sin(theta)
                Nm(j+(i-1)*(nn+1),1) = x
                Nm(j+(i-1)*(nn+1),2) = y
            enddo
        else
            h = (l/sin(theta)) - r
            hn = h/nn
            do j=1,nn+1
                x = (r + hn*(dble(j)-1.0d0))*cos(theta)
                y = (r + hn*(dble(j)-1.0d0))*sin(theta)
                Nm(j+(i-1)*(nn+1),1) = x
                Nm(j+(i-1)*(nn+1),2) = y
            enddo
        endif
    enddo



    open(12,file="node.dat",status="replace")
        write(12,'(i0)')n
        do i=1,n
            write(12,'(f8.5,a,f8.5)')Nm(i,1),' ',Nm(i,2)
        enddo
    close(12)
!*****************************************************

!***********************elem**************************
    ne1 = nn*nn
    ne2 = 4
    mc=0.0d0
    allocate(elem(ne1,ne2))
    allocate(elemx((nn+1)*(nn+1),(nn+1)*(nn+1)))
    elem=0.0d0
    elemx=0.0d0

    do i=1,(nn+1)*(nn+1)
        if(mod(i,nn+1) == 0)then
            mc = mc + 1.0d0
            cycle
        endif
        do j=1,ne2
            if(j <= 2)then
                elemx(i-int(mc),j) = j+i-1
            else
                elemx(i-int(mc),j) = (nn+1)+j+i-1-2
            endif
        enddo
    enddo

    do i=1,ne2
        do j=1,ne1
            elem(j,i) = elemx(j,i)
        enddo
    enddo

    do i=1,ne1
        temp = elem(i,3)
        elem(i,3) = elem(i,4)
        elem(i,4) = temp
    enddo


    open(13,file="elem.dat",status='replace')
        write(13,'(i0,a,i0)')ne1,' ',ne2
        do i=1,ne1
            write(13,'(i0,a,i0,a,i0,a,i0)')elem(i,1),' ',elem(i,2),' ',elem(i,3),' ',elem(i,4)
        enddo
    close(13)
!*****************************************************

!************************load*************************
    n_l = (nn/2)+1 ! 境界条件数（上辺の節点数）
    q = 1.0d0 ! 平板上面にかかる分布荷重（N/m）
    lp = l/dble(nn/2) ! 載荷面における節点間の距離
    p = q*lp/2.0d0 ! 頂点にかかる等価節点力
    allocate(load(n_l,3))
    load = 0.0d0


    do i=1,n_l
        load(i,1) = dble(n_l)*dble(nn+1) + dble(nn+1)*dble(i-1)
    enddo

    do i=1,n_l
        load(i,2) = 2.0d0
    enddo

    do i=1,n_l
        load(i,3) = p
    enddo

    do i=1,n_l-2
        load(i+1,3) = load(i+1,3) + p
    enddo

    open(14,file="load.dat",status="replace")
        write(14,'(i0,a,i0)')n_l,' ',mdf
        do i=1,n_l
            write(14,'(i0,a,i0,a,f7.1)')int(load(i,1)),' ',int(load(i,2)),' ',load(i,3)
        enddo
    close(14)

!*****************************************************

!**********************bc*****************************

    kappa = (3.0d0 - nu) / (1.0d0 + nu)
    mu = dble(E)/(2.0d0*(1+nu))
    allocate(bc_f((nn+1)*2,3))
    bc_f=0.0d0
    pi = 2.0d0 * acos(0.0d0)


    bc1 = (r/(8.0d0*mu))

    do i=1,nn+1
        if(i < (nn/2)+1)then
            theta_bc = ((pi/2.0d0)/nn)*dble(i-1)
            a = l/cos(theta_bc)

            bc2 = a*(kappa+1.0d0)*cos(theta_bc)/r
            bc3 = 2.0d0*r*((1.0d0+kappa)*cos(theta_bc)+cos(theta_bc*3.0d0))/a
            bc4 = 2.0d0*(r**3.0d0)*cos(theta_bc*3.0d0)/(a**3.0d0)
            bc5 = a*(kappa-3.0d0)*sin(theta_bc)/r
            bc6 = 2.0d0*r*((1.0d0-kappa)*sin(theta_bc)+sin(theta_bc*3.0d0))/a
            bc7 = 2.0d0*(r**3.0d0)*sin(theta_bc*3.0d0)/(a**3.0d0)

            bc_f(i*2-1,1) = dble(i*(nn+1))
            bc_f(i*2,1) = dble(i*(nn+1))
            bc_f(i*2-1,2) = 1.0d0
            bc_f(i*2,2) = 2.0d0
            bc_f(i*2-1,3) = bc1*(bc2+bc3-bc4)
            bc_f(i*2,3) = bc1*(bc5+bc6-bc7)
        else
            theta_bc = ((pi/2.0d0)/nn)*dble(i-1)
            a = l/sin(theta_bc)

            bc2 = a*(kappa+1.0d0)*cos(theta_bc)/r
            bc3 = 2.0d0*r*((1.0d0+kappa)*cos(theta_bc)+cos(theta_bc*3.0d0))/a
            bc4 = 2.0d0*(r**3.0d0)*cos(theta_bc*3.0d0)/(a**3.0d0)
            bc5 = a*(kappa-3.0d0)*sin(theta_bc)/r
            bc6 = 2.0d0*r*((1.0d0-kappa)*sin(theta_bc)+sin(theta_bc*3.0d0))/a
            bc7 = 2.0d0*(r**3.0d0)*sin(theta_bc*3.0d0)/(a**3.0d0)

            bc_f(i*2-1,1) = dble(i*(nn+1))
            bc_f(i*2,1) = dble(i*(nn+1))
            bc_f(i*2-1,2) = 1.0d0
            bc_f(i*2,2) = 2.0d0
            bc_f(i*2-1,3) = bc1*(bc2+bc3-bc4)
            bc_f(i*2,3) = bc1*(bc5+bc6-bc7)
        endif
    enddo

    n_b = (nn+1)*4
    allocate(bc(n_b,3))
    bc=0.0d0

    do i=1,nn+1
        bc(i,1) = dble(i)
        bc(i,2) = 2.0d0
    enddo

    do i=1,nn+1
        bc(i+nn+1,1) = dble(i+nn*(nn+1))
        bc(i+nn+1,2) = 1.0d0
    enddo

    do i=1,n_b
        bc(i,3) = 0.0d0
    enddo

    do i=1,(nn+1)*2
        do j=1,3
            bc(i+(nn+1)*2,j) = bc_f(i,j)
        enddo
    enddo

    open(15,file="bc.dat",status="replace")
        write(15,'(i0,a,i0)')n_b,' ',mdf
        do i=1,n_b
            write(15,'(i0,a,i0,a,f19.16)')int(bc(i,1)),' ',int(bc(i,2)),' ',bc(i,3)
        enddo

    close(15)

!*****************************************************

end program main