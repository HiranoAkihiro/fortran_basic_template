program main
    use sub_FEM
    implicit none
    integer(4) :: i,j,n_l,n_b
    real(8) :: eta, xi  ! η→eta, ξ→xi
    real(8) :: Jm(2,2) !　Jマトリックス
    real(8) :: Jminv(2,2) !　Jマトリックスの逆行列
    real(8) :: detJm ! Jマトリックスの行列式
    real(8) :: detJm1,detJm2,detJm3,detJm4 ! 各積分点でのJの行列式格納変数
    real(8) :: Dm(3,3) !　Dマトリックス
    real(8) :: n1(2),n2(2),n3(2),n4(2) !　Bマトリックスの要素（形状関数N₁(n1)～N₄(n4)について、1要素目：（∂N/∂x）、2要素目：（∂N/∂y））
    real(8) :: Bm(3,8) ! Bマトリックス
    real(8) :: BTm(8,3) ! Bマトリックスの転置
    real(8) :: BTDm(8,3) !　[BT][D]
    real(8) :: BTDBm(8,8) !　[BT][D][B](Keの積分する前)
    real(8) :: Ke1m(8,8), Ke2m(8,8), Ke3m(8,8), Ke4m(8,8) !　それぞれの積分点のKe
    real(8) :: Kem(8,8) ! Ke:要素剛性マトリックス

    !***************************境界条件付与***************************************
    integer(4) :: n 
    real(8), allocatable :: bc(:,:) ! ディリクレ境界条件｛節点番号、自由度方向、変位｝
    real(8), allocatable :: load(:,:) ! ノイマン境界条件｛節点番号、自由度方向、力｝
    real(8), allocatable :: A(:,:) !　係数行列
    real(8), allocatable :: b(:) !　右辺ベクトル
    real(8), allocatable :: x(:) !　解ベクトル（ここでは変位ベクトルに相当）
    !*****************************************************************************

    n=8
    allocate(A(n,n))
    allocate(b(n))
    allocate(x(n))


    A=0.0d0
    b=0.0d0
    x=0.0d0

    Kem=0.0d0

    eta = -0.57735d0
    xi = -0.57735d0
    call Jmatrix(eta,xi,Jm)
    call inverse(Jminv,Jm,detJm)
    call n1_pd(n1,Jminv,eta,xi)
    call n2_pd(n2,Jminv,eta,xi)
    call n3_pd(n3,Jminv,eta,xi)
    call n4_pd(n4,Jminv,eta,xi)
    call Bmatrix(Bm,n1,n2,n3,n4)
    call Dmatrix(Dm)
    call B_transposed(BTm,Bm)
    call BT_D_calculation(BTDm,Dm,BTm)
    call BTD_B_calculation(BTDBm,BTDm,Bm)
    detJm1=detJm
    Ke1m=0.0d0
    do i=1,8
        do j=1,8
            Ke1m(i,j) = BTDBm(i,j)*detJm1
        enddo
    enddo

    eta = -0.57735d0
    xi = 0.57735d0
    call Jmatrix(eta,xi,Jm)
    call inverse(Jminv,Jm,detJm)
    call n1_pd(n1,Jminv,eta,xi)
    call n2_pd(n2,Jminv,eta,xi)
    call n3_pd(n3,Jminv,eta,xi)
    call n4_pd(n4,Jminv,eta,xi)
    call Bmatrix(Bm,n1,n2,n3,n4)
    call Dmatrix(Dm)
    call B_transposed(BTm,Bm)
    call BT_D_calculation(BTDm,Dm,BTm)
    call BTD_B_calculation(BTDBm,BTDm,Bm)
    detJm2=detJm
    Ke2m=0.0d0
    do i=1,8
        do j=1,8
            Ke2m(i,j) = BTDBm(i,j)*detJm2
        enddo
    enddo

    eta = 0.57735d0
    xi = 0.57735d0
    call Jmatrix(eta,xi,Jm)
    call inverse(Jminv,Jm,detJm)
    call n1_pd(n1,Jminv,eta,xi)
    call n2_pd(n2,Jminv,eta,xi)
    call n3_pd(n3,Jminv,eta,xi)
    call n4_pd(n4,Jminv,eta,xi)
    call Bmatrix(Bm,n1,n2,n3,n4)
    call Dmatrix(Dm)
    call B_transposed(BTm,Bm)
    call BT_D_calculation(BTDm,Dm,BTm)
    call BTD_B_calculation(BTDBm,BTDm,Bm)
    detJm3=detJm
    Ke3m=0.0d0
    do i=1,8
        do j=1,8
            Ke3m(i,j) = BTDBm(i,j)*detJm3
        enddo
    enddo

    eta = 0.57735d0
    xi = -0.57735d0
    call Jmatrix(eta,xi,Jm)
    call inverse(Jminv,Jm,detJm)
    call n1_pd(n1,Jminv,eta,xi)
    call n2_pd(n2,Jminv,eta,xi)
    call n3_pd(n3,Jminv,eta,xi)
    call n4_pd(n4,Jminv,eta,xi)
    call Bmatrix(Bm,n1,n2,n3,n4)
    call Dmatrix(Dm)
    call B_transposed(BTm,Bm)
    call BT_D_calculation(BTDm,Dm,BTm)
    call BTD_B_calculation(BTDBm,BTDm,Bm)
    detJm4=detJm
    Ke4m=0.0d0
    do i=1,8
        do j=1,8
            Ke4m(i,j) = BTDBm(i,j)*detJm4
        enddo
    enddo
    
    do i=1,8
        do j=1,8
            Kem(i,j) = Ke1m(i,j) + Ke2m(i,j) + Ke3m(i,j) + Ke4m(i,j)
        enddo
    enddo

    A = Kem

!****************ノイマン境界条件の付与***********************
    call read_load(load,n_l)
    do i=1,n_l
        b(int(load(i,1))*2-(2-int(load(i,2)))) = load(i,3)
    enddo
!***********************************************************

!******************ディリクレ境界条件の付与***********************
    call read_bc(bc,n_b)
    do i=1,n_b
        x(int(bc(i,1))*2-(2-int(bc(i,2)))) = bc(i,3)
        do j=1,n
            A(j,(int(bc(i,1))*2)-(2-int(bc(i,2)))) = 0.0d0
            A((int(bc(i,1))*2)-(2-int(bc(i,2))),j) = 0.0d0
        enddo
        A(int(bc(i,1))*2-(2-int(bc(i,2))),int(bc(i,1))*2-(2-int(bc(i,2)))) = 1.0d0
    enddo
!***************************************************************

    write(*,*)'剛性行列(境界条件付与前)'

    do i=1,8
        write(*,*)Kem(i,1:8)
    enddo

    write(*,*)'剛性行列A（境界条件付与後）'

    do i=1,8
        write(*,*)A(i,1:8)
    enddo
    
    write(*,*)'右辺ベクトル(境界条件付与後)'
    write(*,*)b(1:8)

    call linear_solver_CG(A,b,x,n)

    write(*,*)'変位ベクトル（解ベクトル）'
    write(*,*)x(1:8)


!***********************************vtkファイル出力部分*******************************************
    ! open(30,file="square_plate.vtk",status="replace")
    !     write(30,*)'# vtk DataFile Version 4.1'
    !     write(30,*)'Title'
    !     write(30,*)'ASCII'
    !     write(30,*)'DATASET UNSTRUCTURED_GRID'
    !     write(30,*)''
    !     write(30,*)'POINTS 4 float'
    !     write(30,*)Nm(1,1),Nm(1,2)
    !     write(30,*)Nm(2,1),Nm(2,2)
    !     write(30,*)Nm(3,1),Nm(3,2)
    !     write(30,*)Nm(4,1),Nm(4,2)
    !     write(30,*)''
    !     write(30,*)'CELLS 1 5'
    !     write(30,*)'4 0 1 2 3'
    !     write(30,*)''
    !     write(30,*)'CELL_TYPES 1'
    !     write(30,*)'9'
    ! close(30)

!************************************************************************************************
end program main