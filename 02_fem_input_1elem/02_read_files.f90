module read_files
    implicit none
    
contains
    !********read_input（解析パラメータの読み込み）*************
subroutine read_input(E,nu)
    implicit none
    real(8) :: E,nu
    open(10,file="input.dat",status="old")
        read(10,*)E
        read(10,*)nu
    close(10)
end subroutine read_input

!*********************************************************

!**************read_node（節点読み込み）*******************
subroutine read_node(Nm,nn)
    implicit none
    integer(4) :: i,nn
    real(8),allocatable :: Nm(:,:)
    open(11,file="node.dat",status="old")
        read(11,*)nn
        allocate(Nm(nn,2))
        
        do i=1,nn
            read(11,*)Nm(i,1),Nm(i,2)
        enddo
    close(11)

end subroutine read_node

!*********************************************************

!******************read_elem（要素の読み込み）*************
        !subroutine read_elem()
        !    implicit none
        !    real(8), allocatable :: elem(:)
        !    integer(4), allocatable :: m(:)
        !    allocate(m(2))  !elem.datの一行目を１次元配列で読み込むため
        !    open(12,file="elem.dat",status="old")
        !        read(12,*)m
        !        allocate(elem(m(2)))
        !        read(12,*)elem(1),elem(2),elem(3),elem(4) !行をdoループで読み込む方法がわからない
        !        write(*,*)elem(1),elem(2),elem(3),elem(4)
        !    close(12)
        !end subroutine read_elem

!*********************************************************

!*********read_bc（ディリクレ境界条件の読み込み）***********
        subroutine read_bc(bc,n_b)
           implicit none
           integer(4) :: i,n_b,n_mdf_b
           real(8), allocatable :: bc(:,:)
           open(13,file="bc.dat",status="old")
               read(13,*)n_b,n_mdf_b !｛境界条件数｝｛最大自由度数｝
               allocate(bc(n_b,3)) ! ｛節点id｝｛自由度方向｝｛変位値｝
               bc=0.0d0
               do i=1,n_b
                   read(13,*)bc(i,1),bc(i,2),bc(i,3)
               enddo
           close(13)
        end subroutine read_bc

!*********************************************************

!***********read_load（ノイマン境界条件の読み込み）*********
        subroutine read_load(load,n_l)
          implicit none
           integer(4) :: i, n_l,n_mdf_l
           real(8), allocatable :: load(:,:)
           open(14,file="load.dat",status="old")
               read(14,*)n_l,n_mdf_l
               allocate(load(n_l,3))
               do i=1,n_l
                   read(14,*)load(i,1),load(i,2),load(i,3)
               enddo
           close(14)
        end subroutine read_load

!*********************************************************
end module read_files