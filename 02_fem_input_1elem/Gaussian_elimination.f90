program main
    implicit none
    integer(4) :: i,j,k
    integer(4) :: n
    real(8), allocatable :: A(:,:)
    real(8), allocatable :: b(:)
    real(8), allocatable :: x(:)
    real(8) :: multiplier

    n=2
    allocate(A(n,n))
    allocate(b(n))
    allocate(x(n))

    A(1,1)=6.0d0
    A(1,2)=1.0d0
    !A(1,3)=2.0d0
    A(2,1)=7.0d0
    A(2,2)=3.0d0
    !A(2,3)=9.0d0
    !A(3,1)=2.0d0
    !A(3,2)=6.0d0
    !A(3,3)=8.0d0

    b(1)=2.0d0
    b(2)=1.0d0
    !b(3)=0.0d0



!******************係数行列（A）、右辺ベクトル（b）前進消去**********************
    do i=1,n-1
        do j=i+1,n
            
            multiplier = A(j,i)/A(i,i)
            do k=i,n
                A(j,k) = A(j,k)-multiplier*A(i,k)
            enddo
            b(j) = b(j)-multiplier*b(i)
        enddo
    enddo
!******************************************************************************

!********************************後退代入***************************************
    do i=n,1,-1
        x(i) = b(i)/A(i,i)
        do j=i+1,n
            x(i) = x(i) - A(i,j)*x(j)/A(i,i)
        end do
    end do
!*******************************************************************************


    do i=1,n
        write(*,*)A(i,1),A(i,2)!,A(i,3),A(i,4)
    enddo

    do i=1,n
        write(*,*)b(i)
    enddo

    do i=1,n
        write(*,*)x(i)
    enddo

    end program main