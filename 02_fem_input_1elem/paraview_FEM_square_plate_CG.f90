program main
    implicit none
    integer(4) :: i,j,n_l(2),n_b(2)
    real(8) :: eta, xi  ! η→eta, ξ→xi
    real(8),allocatable :: Nm(:,:)
    real(8) :: Jm(2,2) !　Jマトリックス
    real(8) :: Jminv(2,2) !　Jマトリックスの逆行列
    real(8) :: detJm ! Jマトリックスの行列式
    real(8) :: detJm1,detJm2,detJm3,detJm4 ! 各積分点でのJの行列式格納変数
    real(8) :: Dm(3,3) !　Dマトリックス
    real(8) :: n1(2),n2(2),n3(2),n4(2) !　Bマトリックスの要素（形状関数N₁(n1)～N₄(n4)について、1要素目：（∂N/∂x）、2要素目：（∂N/∂y））
    real(8) :: Bm(3,8) ! Bマトリックス
    real(8) :: BTm(8,3) ! Bマトリックスの転置
    real(8) :: BTDm(8,3) !　[BT][D]
    real(8) :: BTDBm(8,8) !　[BT][D][B](Keの積分する前)
    real(8) :: Ke1m(8,8), Ke2m(8,8), Ke3m(8,8), Ke4m(8,8) !　それぞれの積分点のKe
    real(8) :: Kem(8,8) ! Ke:要素剛性マトリックス

    !*******************境界条件付与＋線形ソルバ部分宣言****************************
    integer(4) :: n 
    real(8), allocatable :: bc(:,:) ! ディリクレ境界条件｛節点番号、自由度方向、変位｝
    real(8), allocatable :: load(:,:) ! ノイマン境界条件｛節点番号、自由度方向、力｝
    real(8), allocatable :: A(:,:) !　係数行列
    real(8), allocatable :: b(:) !　右辺ベクトル
    real(8), allocatable :: x(:) !　解ベクトル（ここでは変位ベクトルに相当）
    real(8), allocatable :: r(:) !　残差ベクトル
    real(8), allocatable :: p(:) !　
    real(8), allocatable :: q(:) !　
    real(8) :: bnrm2,rho,beta,rho1,dnrm2,c1,alpha,resid !　
    real(8) :: eps
    integer(4) :: iter,itermax
    itermax=1000
    eps=1.0E-8
    !*****************************************************************************

    n=8
    allocate(A(n,n))
    allocate(b(n))
    allocate(x(n))
    allocate(r(n))
    allocate(p(n))
    allocate(q(n))

    A=0.0d0
    b=0.0d0
    x=0.0d0

    Kem=0.0d0

    eta = -0.57735d0
    xi = -0.57735d0
    call Jmatrix(eta,xi,Jm)
    call inverse(Jminv,Jm,detJm)
    call n1_pd(n1,Jminv,eta,xi)
    call n2_pd(n2,Jminv,eta,xi)
    call n3_pd(n3,Jminv,eta,xi)
    call n4_pd(n4,Jminv,eta,xi)
    call Bmatrix(Bm,n1,n2,n3,n4)
    call Dmatrix(Dm)
    call B_transposed(BTm,Bm)
    call BT_D_calculation(BTDm,Dm,BTm)
    call BTD_B_calculation(BTDBm,BTDm,Bm)
    detJm1=detJm
    Ke1m=0.0d0
    do i=1,8
        do j=1,8
            Ke1m(i,j) = BTDBm(i,j)*detJm1
        enddo
    enddo

    eta = -0.57735d0
    xi = 0.57735d0
    call Jmatrix(eta,xi,Jm)
    call inverse(Jminv,Jm,detJm)
    call n1_pd(n1,Jminv,eta,xi)
    call n2_pd(n2,Jminv,eta,xi)
    call n3_pd(n3,Jminv,eta,xi)
    call n4_pd(n4,Jminv,eta,xi)
    call Bmatrix(Bm,n1,n2,n3,n4)
    call Dmatrix(Dm)
    call B_transposed(BTm,Bm)
    call BT_D_calculation(BTDm,Dm,BTm)
    call BTD_B_calculation(BTDBm,BTDm,Bm)
    detJm2=detJm
    Ke2m=0.0d0
    do i=1,8
        do j=1,8
            Ke2m(i,j) = BTDBm(i,j)*detJm2
        enddo
    enddo

    eta = 0.57735d0
    xi = 0.57735d0
    call Jmatrix(eta,xi,Jm)
    call inverse(Jminv,Jm,detJm)
    call n1_pd(n1,Jminv,eta,xi)
    call n2_pd(n2,Jminv,eta,xi)
    call n3_pd(n3,Jminv,eta,xi)
    call n4_pd(n4,Jminv,eta,xi)
    call Bmatrix(Bm,n1,n2,n3,n4)
    call Dmatrix(Dm)
    call B_transposed(BTm,Bm)
    call BT_D_calculation(BTDm,Dm,BTm)
    call BTD_B_calculation(BTDBm,BTDm,Bm)
    detJm3=detJm
    Ke3m=0.0d0
    do i=1,8
        do j=1,8
            Ke3m(i,j) = BTDBm(i,j)*detJm3
        enddo
    enddo

    eta = 0.57735d0
    xi = -0.57735d0
    call Jmatrix(eta,xi,Jm)
    call inverse(Jminv,Jm,detJm)
    call n1_pd(n1,Jminv,eta,xi)
    call n2_pd(n2,Jminv,eta,xi)
    call n3_pd(n3,Jminv,eta,xi)
    call n4_pd(n4,Jminv,eta,xi)
    call Bmatrix(Bm,n1,n2,n3,n4)
    call Dmatrix(Dm)
    call B_transposed(BTm,Bm)
    call BT_D_calculation(BTDm,Dm,BTm)
    call BTD_B_calculation(BTDBm,BTDm,Bm)
    detJm4=detJm
    Ke4m=0.0d0
    do i=1,8
        do j=1,8
            Ke4m(i,j) = BTDBm(i,j)*detJm4
        enddo
    enddo
    
    do i=1,8
        do j=1,8
            Kem(i,j) = Ke1m(i,j) + Ke2m(i,j) + Ke3m(i,j) + Ke4m(i,j)
        enddo
    enddo

    A = Kem

!****************ノイマン境界条件の付与***********************
    call read_load(load,n_l)
    do i=1,n_l(1)
        b(int(load(i,1))*2-(2-int(load(i,2)))) = load(i,3)
    enddo
!***********************************************************

!******************ディリクレ境界条件の付与***********************
    call read_bc(bc,n_b)
    do i=1,n_b(1)
        x(int(bc(i,1))*2-(2-int(bc(i,2)))) = bc(i,3)
        do j=1,n
            A(j,(int(bc(i,1))*2)-(2-int(bc(i,2)))) = 0.0d0
            A((int(bc(i,1))*2)-(2-int(bc(i,2))),j) = 0.0d0
        enddo
        A(int(bc(i,1))*2-(2-int(bc(i,2))),int(bc(i,1))*2-(2-int(bc(i,2)))) = 1.0d0
    enddo
!***************************************************************


!***************************************共役勾配法*******************************************

    do i=1,n
        r(i) = b(i)
        do j=1,n
            r(i) = r(i) - A(i,j)*x(j)
        enddo
    enddo

    bnrm2 = 0.0d0
    do i=1,n
        bnrm2 = bnrm2 + b(i) ** 2.0d0
    enddo

    rho1 = 0.0d0
    do iter=1,itermax

        rho = 0.0d0
        do i=1,n
            rho = rho + r(i)*r(i)
        enddo

        if(iter.eq.1) then
            do i=1,n
                p(i) = r(i)
            enddo
        else
            beta = rho / rho1
            do i=1,n
                p(i) = r(i) + beta * p(i)
            enddo
        endif

        do i=1,n
            q(i) = 0.0d0
            do j=1,n
                q(i) = q(i) + A(i,j) * p(j)
            enddo
        enddo


        c1=0.0d0
        do i=1,n
            c1 = c1 + p(i) * q(i)
        enddo
        alpha = rho / c1

        do i=1,n
            x(i) = x(i) + alpha * p(i)
            r(i) = r(i) - alpha * q(i)
        enddo

        dnrm2 = 0.0d0
        do i=1,n
            dnrm2 = dnrm2 + r(i) ** 2
        enddo

        resid = dsqrt(dnrm2/bnrm2)

        if(resid.le.eps) exit
        rho1 =rho

    enddo

    write(*,*)'    x変位                         y変位'
    do i=1,4
        write(*,*)x(2*i-1),x(2*i)
    enddo

!************************************************************************************************

!***********************************vtkファイル出力部分*******************************************
    call read_node(Nm,n)
    open(30,file="square_plate.vtk",status="replace")
        write(30,'(a)')'# vtk DataFile Version 4.1'
        write(30,'(a)')'Title'
        write(30,'(a)')'ASCII'
        write(30,'(a)')'DATASET UNSTRUCTURED_GRID'
        write(30,*)''
        write(30,'(a,a,i0,a,a)')'POINTS',' ',4,' ','float'
        write(30,'(3f12.6)')Nm(1,1),Nm(1,2),0.0d0
        write(30,'(3f12.6)')Nm(2,1),Nm(2,2),0.0d0
        write(30,'(3f12.6)')Nm(3,1),Nm(3,2),0.0d0
        write(30,'(3f12.6)')Nm(4,1),Nm(4,2),0.0d0
        write(30,*)''
        write(30,'(a,a,i0,a,i0)')'CELLS',' ',1,' ',5
        write(30,'(i0,a,i0,a,i0,a,i0,a,i0)')4,' ',0,' ',1,' ',2,' ',3
        write(30,*)''
        write(30,'(a,a,i0)')'CELL_TYPES',' ',1
        write(30,'(i0)')9
        write(30,*)''
        write(30,'(a,a,i0)')'POINT_DATA',' ',4
        write(30,'(a)')'VECTORS Displacement float'
        write(30,'(3f12.6)')x(1),x(2),0.0d0
        write(30,'(3f12.6)')x(3),x(4),0.0d0
        write(30,'(3f12.6)')x(5),x(6),0.0d0
        write(30,'(3f12.6)')x(7),x(8),0.0d0
    close(30)

!************************************************************************************************
contains

!***********BTD_B_calculation（[BTD][B]の計算）************
subroutine BTD_B_calculation(BTDBm,BTDm,Bm)
    implicit none
    integer(4) :: i,j,k
    real(8) :: BTDBm(8,8)
    real(8) :: Bm(3,8)
    real(8) :: BTDm(8,3)
    BTDBm=0.0d0
    do i=1,8
        do j=1,8
            do k=1,3
                BTDBm(i,j) = BTDBm(i,j) + BTDm(i,k) * Bm(k,j)
            enddo
        enddo
    enddo

end subroutine BTD_B_calculation

!*********************************************************

!**********BT_D_calculation（[BT][D]の計算）***************
subroutine BT_D_calculation(BTDm,Dm,BTm)
    implicit none
    integer(4) :: i,j,k
    real(8) :: BTDm(8,3)
    real(8) :: BTm(8,3)
    real(8) :: Dm(3,3)
    BTDm = 0.0d0
    do i=1,8
        do j=1,3
            do k=1,3
                BTDm(i,j) = BTDm(i,j) + BTm(i,k) * Dm(k,j)
            enddo
        enddo
    enddo
end subroutine BT_D_calculation

!*********************************************************

!************B_transposed（Bの転置計算）*******************
subroutine B_transposed(BTm,Bm)
    implicit none
    integer(4) :: i,j
    real(8) :: BTm(8,3)
    real(8) :: Bm(3,8)
    BTm=0.0d0
    do i=1,3
        do j=1,8
            BTm(j,i)=Bm(i,j)
        enddo
    enddo
end subroutine B_transposed

!*********************************************************

!**************Bmatrix（Bマトリックス）********************
subroutine Bmatrix(Bm,n1,n2,n3,n4)
    implicit none
    integer(4) :: i 
    real(8) :: Bm(3,8)
    real(8) :: n1(2),n2(2),n3(2),n4(2)
    Bm(1,1)=n1(1)
    Bm(1,3)=n2(1)
    Bm(1,5)=n3(1)
    Bm(1,7)=n4(1)
    Bm(2,2)=n1(2)
    Bm(2,4)=n2(2)
    Bm(2,6)=n3(2)
    Bm(2,8)=n4(2)
    Bm(3,1)=n1(2)
    Bm(3,2)=n1(1)
    Bm(3,3)=n2(2)
    Bm(3,4)=n2(1)
    Bm(3,5)=n3(2)
    Bm(3,6)=n3(1)
    Bm(3,7)=n4(2)
    Bm(3,8)=n4(1)
    do i=1,4
        Bm(1,2*i)=0.0d0
        Bm(2,2*i-1)=0.0d0
    enddo
end subroutine Bmatrix

!*********************************************************

!***********Dマトリックスの定義****************************
subroutine Dmatrix(Dm)
    implicit none
    integer(4) :: k,l
    real(8) :: Dm(3,3)
    real(8) :: E,nu
    call read_input(E,nu)
    Dm = 0.0d0
    Dm(1,1) = 1.0d0 / (1.0d0 - nu)
    Dm(1,2) = nu / (1.0d0 - nu)
    Dm(2,1) = nu / (1.0d0 - nu)
    Dm(2,2) = 1.0d0 / (1.0d0 - nu)
    Dm(3,3) = 1.0d0 / 2.0d0
    do k = 1,2
        Dm(k,3) = 0.0d0
        Dm(3,k) = 0.0d0
    enddo
    do k=1,3
        do l=1,3
            Dm(k,l)=Dm(k,l)*(E/(1+nu))
        enddo
    enddo

end subroutine Dmatrix

!*********************************************************


!******************n1（B要素計算）**************************
subroutine n1_pd(n1,Jminv,eta,xi)
    implicit none
    real(8) :: eta, xi
    real(8) :: n1(2)
    real(8) :: Jminv(2,2)
    n1=0.0d0
    n1(1)=(Jminv(1,1) * (-(1.0d0-eta)/4.0d0)) + (Jminv(1,2) * (-(1.0d0-xi)/4.0d0))
    n1(2)=(Jminv(2,1) * (-(1.0d0-eta)/4.0d0)) + (Jminv(2,2) * (-(1.0d0-xi)/4.0d0))
end subroutine n1_pd
!**********************************************************

!******************n2（B要素計算）**************************
subroutine n2_pd(n2,Jminv,eta,xi)
    implicit none
    real(8) :: eta, xi
    real(8) :: n2(2)
    real(8) :: Jminv(2,2)
    n2=0.0d0
    n2(1)=(Jminv(1,1) * (1.0d0-eta)/4.0d0) + (Jminv(1,2) * -(1.0d0+xi)/4.0d0)
    n2(2)=(Jminv(2,1) * (1.0d0-eta)/4.0d0) + (Jminv(2,2) * -(1.0d0+xi)/4.0d0)
end subroutine n2_pd
!**********************************************************

!******************n3（B要素計算）**************************
subroutine n3_pd(n3,Jminv,eta,xi)
    implicit none
    real(8) :: eta, xi
    real(8) :: n3(2)
    real(8) :: Jminv(2,2)
    n3=0.0d0
    n3(1)=(Jminv(1,1) * (1.0d0+eta)/4.0d0) + (Jminv(1,2) * (1.0d0+xi)/4.0d0)
    n3(2)=(Jminv(2,1) * (1.0d0+eta)/4.0d0) + (Jminv(2,2) * (1.0d0+xi)/4.0d0)
end subroutine n3_pd
!**********************************************************

!******************n4（B要素計算）**************************
subroutine n4_pd(n4,Jminv,eta,xi)
    implicit none
    real(8) :: eta, xi
    real(8) :: n4(2)
    real(8) :: Jminv(2,2)
    n4=0.0d0
    n4(1)=(Jminv(1,1) * -(1.0d0+eta)/4.0d0) + (Jminv(1,2) * (1.0d0-xi)/4.0d0)
    n4(2)=(Jminv(2,1) * -(1.0d0+eta)/4.0d0) + (Jminv(2,2) * (1.0d0-xi)/4.0d0)
end subroutine n4_pd
!**********************************************************



!****************inverse（J逆行列計算）*********************
subroutine inverse(Jminv,Jm,detJm)
    implicit none
    real(8) :: j11, j12, j21, j22, detJm
    real(8) :: Jminv(2,2)
    real(8) :: Jm(2,2)

    j11 = Jm(1,1)
    j12 = Jm(1,2)
    j21 = Jm(2,1)
    j22 = Jm(2,2)

    detJm = 0.0d0

    detJm = j11 * j22 - j12 * j21

    Jminv(1,1) = j22 / detJm
    Jminv(1,2) = -(j12 / detJm)
    Jminv(2,1) = -(j21 / detJm)
    Jminv(2,2) = j11 / detJm
end subroutine inverse

!*********************************************************

!*****************Jマトリックスの演算**********************
subroutine Jmatrix(eta,xi,Jm)
    implicit none
    integer(4) :: k,l,m
    real(8) :: s(2,4)
    real(8) :: Jm(2,2)
    real(8) :: eta, xi 
    real(8), allocatable :: Nm(:,:)
    integer(4) :: n
    s=0.0d0
    call read_node(Nm,n)
    Jm=0.0d0
    s(1,1)=-(1.0d0-eta)/4.0d0
    s(1,2)=(1.0d0-eta)/4.0d0
    s(1,3)=(1.0d0+eta)/4.0d0
    s(1,4)=-(1.0d0+eta)/4.0d0
    s(2,1)=-(1.0d0-xi)/4.0d0
    s(2,2)=-(1.0d0+xi)/4.0d0
    s(2,3)=(1.0d0+xi)/4.0d0
    s(2,4)=(1.0d0-xi)/4.0d0
    do k=1,2
        do l=1,2
            do m=1,n
                Jm(k,l) = Jm(k,l) + s(k,m) * Nm(m,l)
            enddo
        enddo
    enddo
end subroutine Jmatrix

!*********************************************************

!********read_input（解析パラメータの読み込み）*************
subroutine read_input(E,nu)
    implicit none
    real(8) :: E,nu
    open(10,file="input.dat",status="old")
        read(10,*)E
        read(10,*)nu
    close(10)
end subroutine read_input

!*********************************************************

!**************read_node（節点読み込み）*******************
subroutine read_node(Nm,n)
    implicit none
    integer(4) :: i,n
    real(8),allocatable :: Nm(:,:)
    open(11,file="node.dat",status="old")
        read(11,*)n
        allocate(Nm(n,2))
        
        do i=1,n
            read(11,*)Nm(i,1),Nm(i,2)
        enddo
    close(11)

end subroutine read_node

!*********************************************************

!******************read_elem（要素の読み込み）*************
        !subroutine read_elem()
        !    implicit none
        !    real(8), allocatable :: elem(:)
        !    integer(4), allocatable :: m(:)
        !    allocate(m(2))  !elem.datの一行目を１次元配列で読み込むため
        !    open(12,file="elem.dat",status="old")
        !        read(12,*)m
        !        allocate(elem(m(2)))
        !        read(12,*)elem(1),elem(2),elem(3),elem(4) !行をdoループで読み込む方法がわからない
        !        write(*,*)elem(1),elem(2),elem(3),elem(4)
        !    close(12)
        !end subroutine read_elem

!*********************************************************

!*********read_bc（ディリクレ境界条件の読み込み）***********
        subroutine read_bc(bc,n_b)
           implicit none
           integer(4) :: i,n_b(2)
           real(8), allocatable :: bc(:,:)
           open(13,file="bc.dat",status="old")
               read(13,*)n_b !｛境界条件数｝｛最大自由度数｝
               allocate(bc(n_b(1),n_b(2) + 1)) ! ｛節点id｝｛自由度方向｝｛変位値｝
               bc=0.0d0
               do i=1,n_b(1)
                   read(13,*)bc(i,1),bc(i,2),bc(i,3)
               enddo
           close(13)
        end subroutine read_bc

!*********************************************************

!***********read_load（ノイマン境界条件の読み込み）*********
        subroutine read_load(load,n_l)
          implicit none
           integer(4) :: i, n_l(2)
           real(8), allocatable :: load(:,:)
           open(14,file="load.dat",status="old")
               read(14,*)n_l
               allocate(load(n_l(1),3))
               do i=1,n_l(1) 
                   read(14,*)load(i,1),load(i,2),load(i,3)
               enddo
           close(14)
        end subroutine read_load

!*********************************************************

end program main