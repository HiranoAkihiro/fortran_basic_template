program main
    implicit none
    integer(4) :: i, m(2)
    real(8), allocatable :: load(:,:)
    open(14,file="load.dat",status="old")
        read(14,*)m
        allocate(load(m(1),3))
        do i=1,m(1) 
            read(14,*)load(i,1),load(i,2),load(i,3)
            write(*,*)load(i,1),load(i,2),load(i,3)
        enddo
        write(*,*) load(1,1)
        write(*,*) int(load(1,1))
    close(14)

end program main
