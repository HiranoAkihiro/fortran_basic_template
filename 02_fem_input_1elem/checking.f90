program forward_elimination
    implicit none
    integer, parameter :: n = 3 ! 行列の次元
    integer :: i, j, k
    real(kind=8) :: A(n,n), b(n), factor
  
    ! 係数行列Aと右辺ベクトルbの設定
    A = reshape([2., 1., -1., -3., -1., 2., -2., 1., 2.], [n,n])
    b = [8., -11., -3.]
  
    ! 前進消去
    do k = 1, n-1
      do i = k+1, n
        factor = A(i,k) / A(k,k)
        do j = k+1, n
          A(i,j) = A(i,j) - factor * A(k,j)
        end do
        A(i,k) = factor
        b(i) = b(i) - factor * b(k)
      end do
    end do
  
    ! 前進消去前のAの出力
    print *, "Forward Elimination: Before"
    do i = 1, n
      write (*, '(10f8.3)') A(i,:)
    end do
    write (*, '(10f8.3)') b
  
    ! 前進消去後のAの出力
    print *, "Forward Elimination: After"
    do i = 1, n
      write (*, '(10f8.3)') A(i,:i)
    end do
    write (*, '(10f8.3)') b
  
  end program forward_elimination
  