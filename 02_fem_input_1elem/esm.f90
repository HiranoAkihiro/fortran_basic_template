program main
    implicit none
    integer(4) :: i,k,l
    real(8) :: eta, xi  ! η→eta, ξ→xi
    real(8) :: Dm(3,3) !　Dマトリックス
    real(8) :: Jm(2,2) !　Jマトリックス
    real(8) :: Jminv(2,2) !　Jマトリックスの逆行列
    real(8) :: n1(2),n2(2),n3(2),n4(2) !　Bマトリックスの要素
    real(8) :: Bm(3,8) ! Bマトリックス
    real(8) :: BTm(8,3) ! Bマトリックスの転置
    real(8) :: BTDm(8,3) !　[BT][D]
    real(8) :: BTDBm(8,8) !　[BT][D][B](Keの積分する前)
    real(8) :: Ke1m(8,8), Ke2m(8,8), Ke3m(8,8), Ke4m(8,8) !　それぞれの積分点のKe
    real(8) :: Kem(8,8) ! Ke:要素剛性マトリックス

    Kem=0.0d0

    eta = -0.57735d0
    xi = -0.57735d0
    call Jmatrix(eta,xi,Jm)
    call inverse(Jminv,Jm)
    call n1_pd(n1,Jminv,eta,xi)
    call n2_pd(n2,Jminv,eta,xi)
    call n3_pd(n3,Jminv,eta,xi)
    call n4_pd(n4,Jminv,eta,xi)
    call Bmatrix(Bm,n1,n2,n3,n4)
    call Dmatrix(Dm)
    call B_transposed(BTm,Bm)
    call BT_D_calculation(BTDm,Dm,BTm)
    call BTD_B_calculation(BTDBm,BTDm,Bm)
    Ke1m=0.0d0
    do k=1,8
        do l=1,8
            Ke1m(k,l) = BTDBm(k,l)
        enddo
    enddo
    
    eta = -0.57735d0
    xi = 0.57735d0
    call Jmatrix(eta,xi,Jm)
    call inverse(Jminv,Jm)
    call n1_pd(n1,Jminv,eta,xi)
    call n2_pd(n2,Jminv,eta,xi)
    call n3_pd(n3,Jminv,eta,xi)
    call n4_pd(n4,Jminv,eta,xi)
    call Bmatrix(Bm,n1,n2,n3,n4)
    call Dmatrix(Dm)
    call B_transposed(BTm,Bm)
    call BT_D_calculation(BTDm,Dm,BTm)
    call BTD_B_calculation(BTDBm,BTDm,Bm)
    Ke2m=0.0d0
    do k=1,8
        do l=1,8
            Ke2m(k,l) = BTDBm(k,l)
        enddo
    enddo

    eta = 0.57735d0
    xi = 0.57735d0
    call Jmatrix(eta,xi,Jm)
    call inverse(Jminv,Jm)
    call n1_pd(n1,Jminv,eta,xi)
    call n2_pd(n2,Jminv,eta,xi)
    call n3_pd(n3,Jminv,eta,xi)
    call n4_pd(n4,Jminv,eta,xi)
    call Bmatrix(Bm,n1,n2,n3,n4)
    call Dmatrix(Dm)
    call B_transposed(BTm,Bm)
    call BT_D_calculation(BTDm,Dm,BTm)
    call BTD_B_calculation(BTDBm,BTDm,Bm)
    Ke3m=0.0d0
    do k=1,8
        do l=1,8
            Ke3m(k,l) = BTDBm(k,l)
        enddo
    enddo

    eta = 0.57735d0
    xi = -0.57735d0
    call Jmatrix(eta,xi,Jm)
    call inverse(Jminv,Jm)
    call n1_pd(n1,Jminv,eta,xi)
    call n2_pd(n2,Jminv,eta,xi)
    call n3_pd(n3,Jminv,eta,xi)
    call n4_pd(n4,Jminv,eta,xi)
    call Bmatrix(Bm,n1,n2,n3,n4)
    call Dmatrix(Dm)
    call B_transposed(BTm,Bm)
    call BT_D_calculation(BTDm,Dm,BTm)
    call BTD_B_calculation(BTDBm,BTDm,Bm)
    Ke4m=0.0d0
    do k=1,8
        do l=1,8
            Ke4m(k,l) = BTDBm(k,l)
        enddo
    enddo
    
    do k=1,8
        do l=1,8
            Kem(k,l) = Ke1m(k,l) + Ke2m(k,l) + Ke3m(k,l) + Ke4m(k,l)
        enddo
    enddo

    do i=1,8
        write(*,*)Kem(i,1:8)
    enddo

contains

!***********BTD_B_calculation（[BTD][B]の計算）************
        subroutine BTD_B_calculation(BTDBm,BTDm,Bm)
            implicit none
            integer(4) :: i,j,k
            real(8) :: BTDBm(8,8)
            real(8) :: Bm(3,8)
            real(8) :: BTDm(8,3)
            !call Bmatrix(Bm)
            !call BT_D_calculation(BTDm)
            BTDBm=0.0d0
            do i=1,8
                do j=1,8
                    do k=1,3
                        BTDBm(i,j) = BTDBm(i,j) + BTDm(i,k) * Bm(k,j)
                    enddo
                enddo
            enddo

        end subroutine BTD_B_calculation

!*********************************************************

!**********BT_D_calculation（[BT][D]の計算）***************
        subroutine BT_D_calculation(BTDm,Dm,BTm)
            implicit none
            integer(4) :: i,j,k
            real(8) :: BTDm(8,3)
            real(8) :: BTm(8,3)
            real(8) :: Dm(3,3)
            !call Dmatrix(Dm)
            !call B_transposed(BTm)
            BTDm = 0.0d0
            do i=1,8
                do j=1,3
                    do k=1,3
                        BTDm(i,j) = BTDm(i,j) + BTm(i,k) * Dm(k,j)
                    enddo
                enddo
            enddo
        end subroutine BT_D_calculation

!*********************************************************

!************B_transposed（Bの転置計算）*******************
        subroutine B_transposed(BTm,Bm)
            implicit none
            integer(4) :: i,j
            real(8) :: BTm(8,3)
            real(8) :: Bm(3,8)
            BTm=0.0d0
            !call Bmatrix(Bm)
            do i=1,3
                do j=1,8
                    BTm(j,i)=Bm(i,j)
                enddo
            enddo
        end subroutine B_transposed

!*********************************************************

!**************Bmatrix（Bマトリックス）********************
        subroutine Bmatrix(Bm,n1,n2,n3,n4)
            implicit none
            integer(4) :: i 
            real(8) :: Bm(3,8)
            real(8) :: n1(2),n2(2),n3(2),n4(2)
            !allocate(B(3,8))
            !call n1_pd(n1)
            !call n2_pd(n2)
            !call n3_pd(n3)
            !call n4_pd(n4)
            Bm(1,1)=n1(1)
            Bm(1,3)=n2(1)
            Bm(1,5)=n3(1)
            Bm(1,7)=n4(1)
            Bm(2,2)=n1(2)
            Bm(2,4)=n2(2)
            Bm(2,6)=n3(2)
            Bm(2,8)=n4(2)
            Bm(3,1)=n1(2)
            Bm(3,2)=n1(1)
            Bm(3,3)=n2(2)
            Bm(3,4)=n2(1)
            Bm(3,5)=n3(2)
            Bm(3,6)=n3(1)
            Bm(3,7)=n4(2)
            Bm(3,8)=n4(1)
            do i=1,4
                Bm(1,2*i)=0.0d0
                Bm(2,2*i-1)=0.0d0
            enddo
        end subroutine Bmatrix

!*********************************************************

!***********Dマトリックスの定義****************************
        subroutine Dmatrix(Dm)
            implicit none
            integer(4) :: k,l
            real(8) :: Dm(3,3)
            real(8) :: E,nu
            call read_input(E,nu)
            Dm = 0.0d0
            Dm(1,1) = 1.0d0 / (1.0d0 - nu)
            Dm(1,2) = nu / (1.0d0 - nu)
            Dm(2,1) = nu / (1.0d0 - nu)
            Dm(2,2) = 1.0d0 / (1.0d0 - nu)
            Dm(3,3) = 1.0d0 / 2.0d0
            do k = 1,2
                Dm(k,3) = 0.0d0
                Dm(3,k) = 0.0d0
            enddo
            do k=1,3
                do l=1,3
                    Dm(k,l)=Dm(k,l)*(E/(1+nu))
                enddo
            enddo

        end subroutine Dmatrix

!*********************************************************


!******************n1（B要素計算）**************************
        subroutine n1_pd(n1,Jminv,eta,xi)
            implicit none
            real(8) :: eta, xi
            real(8) :: n1(2)
            real(8) :: Jminv(2,2)
            n1=0.0d0
            !call inverse(Jminv)
            n1(1)=(Jminv(1,1) * (-(1.0d0-eta)/4.0d0)) + (Jminv(1,2) * (-(1.0d0-xi)/4.0d0))
            n1(2)=(Jminv(2,1) * (-(1.0d0-eta)/4.0d0)) + (Jminv(2,2) * (-(1.0d0-xi)/4.0d0))
        end subroutine n1_pd
!**********************************************************

!******************n2（B要素計算）**************************
        subroutine n2_pd(n2,Jminv,eta,xi)
            implicit none
            real(8) :: eta, xi
            real(8) :: n2(2)
            real(8) :: Jminv(2,2)
            n2=0.0d0
            !call inverse(Jminv)
            n2(1)=(Jminv(1,1) * (1.0d0-eta)/4.0d0) + (Jminv(1,2) * -(1.0d0+xi)/4.0d0)
            n2(2)=(Jminv(2,1) * (1.0d0-eta)/4.0d0) + (Jminv(2,2) * -(1.0d0+xi)/4.0d0)
        end subroutine n2_pd
!**********************************************************

!******************n3（B要素計算）**************************
        subroutine n3_pd(n3,Jminv,eta,xi)
            implicit none
            real(8) :: eta, xi
            real(8) :: n3(2)
            real(8) :: Jminv(2,2)
            n3=0.0d0
            !call inverse(Jminv)
            n3(1)=(Jminv(1,1) * (1.0d0+eta)/4.0d0) + (Jminv(1,2) * (1.0d0+xi)/4.0d0)
            n3(2)=(Jminv(2,1) * (1.0d0+eta)/4.0d0) + (Jminv(2,2) * (1.0d0+xi)/4.0d0)
        end subroutine n3_pd
!**********************************************************

!******************n4（B要素計算）**************************
        subroutine n4_pd(n4,Jminv,eta,xi)
            implicit none
            real(8) :: eta, xi
            real(8) :: n4(2)
            real(8) :: Jminv(2,2)
            n4=0.0d0
            !call inverse(Jminv)
            n4(1)=(Jminv(1,1) * -(1.0d0+eta)/4.0d0) + (Jminv(1,2) * (1.0d0-xi)/4.0d0)
            n4(2)=(Jminv(2,1) * -(1.0d0+eta)/4.0d0) + (Jminv(2,2) * (1.0d0-xi)/4.0d0)
        end subroutine n4_pd
!**********************************************************



!****************inverse（J逆行列計算）*********************
        subroutine inverse(Jminv,Jm)
            implicit none
            real(8) :: j11, j12, j21, j22, detJm
            real(8) :: Jminv(2,2)
            real(8) :: Jm(2,2)
            !call Jmatrix(Jm)
        
            j11 = Jm(1,1)
            j12 = Jm(1,2)
            j21 = Jm(2,1)
            j22 = Jm(2,2)
        
            detJm = j11 * j22 - j12 * j21
        
            Jminv(1,1) = j22 / detJm
            Jminv(1,2) = -(j12 / detJm)
            Jminv(2,1) = -(j21 / detJm)
            Jminv(2,2) = j11 / detJm
        end subroutine inverse

!*********************************************************

!*****************Jマトリックスの演算**********************
        subroutine Jmatrix(eta,xi,Jm)
            implicit none
            integer(4) :: k,l,m
            real(8) :: s(2,4)
            real(8) :: Jm(2,2)
            real(8) :: eta, xi 
            real(8), allocatable :: Nm(:,:)
            integer(4) :: n
            s=0.0d0
            call read_node(Nm,n)
            !allocate(J(2,2))
            Jm=0.0d0
            s(1,1)=-(1.0d0-eta)/4.0d0
            s(1,2)=(1.0d0-eta)/4.0d0
            s(1,3)=(1.0d0+eta)/4.0d0
            s(1,4)=-(1.0d0+eta)/4.0d0
            s(2,1)=-(1.0d0-xi)/4.0d0
            s(2,2)=-(1.0d0+xi)/4.0d0
            s(2,3)=(1.0d0+xi)/4.0d0
            s(2,4)=(1.0d0-xi)/4.0d0
            do k=1,2
                do l=1,2
                    do m=1,n
                        Jm(k,l) = Jm(k,l) + s(k,m) * Nm(m,l)
                    enddo
                enddo
            enddo
        end subroutine Jmatrix

!*********************************************************

!********read_input（解析パラメータの読み込み）*************
        subroutine read_input(E,nu)
            implicit none
            real(8) :: E,nu
            open(10,file="input.dat",status="old")
                read(10,*)E
                read(10,*)nu
            close(10)
            !write(*,*)E,nu
        end subroutine read_input

!*********************************************************
    
!**************read_node（節点読み込み）*******************
        subroutine read_node(Nm,n)
            implicit none
            integer(4) :: i,n
            real(8),allocatable :: Nm(:,:)
            open(11,file="node.dat",status="old")
                read(11,*)n
                allocate(Nm(n,2))
                
                do i=1,n
                    read(11,*)Nm(i,1),Nm(i,2)
                enddo
            close(11)

        end subroutine read_node

!*********************************************************

!******************read_elem（要素の読み込み）*************
        !subroutine read_elem()
        !    implicit none
        !    real(8), allocatable :: elem(:)
        !    integer(4), allocatable :: m(:)
        !    allocate(m(2))  !elem.datの一行目を１次元配列で読み込むため
        !    open(12,file="elem.dat",status="old")
        !        read(12,*)m
        !        allocate(elem(m(2)))
        !        read(12,*)elem(1),elem(2),elem(3),elem(4) !行をdoループで読み込む方法がわからない
        !        write(*,*)elem(1),elem(2),elem(3),elem(4)
        !    close(12)
        !end subroutine read_elem

!*********************************************************

!*********read_bc（ディリクレ境界条件の読み込み）***********
        !subroutine read_bc()
        !    implicit none
        !    integer(4) :: i,m(2)
        !    real(8), allocatable :: bc(:,:)
        !    open(13,file="bc.dat",status="old")
        !        read(13,*)m !｛境界条件数｝｛最大自由度数｝
        !        allocate(bc(m(1),m(2) + 1)) ! ｛節点id｝｛自由度方向｝｛変位値｝
        !        bc=0.0d0
        !        do i=1,m(1)
        !            read(13,*)bc(i,1),bc(i,2),bc(i,3)
        !            write(*,*)bc(i,1),bc(i,2),bc(i,3)
        !        enddo
        !    close(13)
        !end subroutine read_bc

!*********************************************************

!***********read_load（ノイマン境界条件の読み込み）*********
        !subroutine read_load()
        !   implicit none
        !    integer(4) :: i, m(2)
        !    real(8), allocatable :: load(:,:)
        !    open(14,file="load.dat",status="old")
        !        read(14,*)m
        !        allocate(load(m(1),3))
        !        do i=1,m(1) 
        !            read(14,*)load(i,1),load(i,2),load(i,3)
        !            write(*,*)load(i,1),load(i,2),load(i,3)
        !        enddo
        !    close(14)
        !end subroutine read_load

!*********************************************************

end program main